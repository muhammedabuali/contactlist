package com.example.contactlist.fragments;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.example.contactlist.R;
import com.example.contactlist.data.Contact;
import com.example.contactlist.helpers.DataHelper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class ContactsFragment extends Fragment {

    // TODO: Customize parameters
    private int mColumnCount = 1;

    private OnListFragmentInteractionListener mListener;
    private ArrayList<Contact> contactList;
    private RecyclerView recyclerView;
    private static final String CONTACTS_SORTED_STATE = "isContactsSorted";
    private String contactsSorted = "";
    private static final String Sorted_Asc ="asc";
    private static final String Sorted_Des ="des";

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ContactsFragment() {
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // receive data
        contactList = getContactsData();

        // include options menu
        setHasOptionsMenu(true);

        // if landscape orientation make layout as a grid
        try {
            int orientation = getResources().getConfiguration().orientation;
            if (orientation == Configuration.ORIENTATION_LANDSCAPE){
                mColumnCount = 2;
            }else {
                mColumnCount = 1;
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user_list, container, false);

        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            recyclerView = (RecyclerView) view;
            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }
            ArrayList<Contact> contacts = getContactsData();
            recyclerView.setAdapter(new UserRecyclerViewAdapter(contacts, mListener));

            // restore contacts sort state
            // todo store contacts outside of fragment class
            if (savedInstanceState != null){
                String state = savedInstanceState.getString(CONTACTS_SORTED_STATE, "");
                if (state != null){
                    contactsSorted = state;
                }
                if (contacts != null && contacts.size() > 0){
                    if (contactsSorted.equals(Sorted_Asc)){
                        sortContacts(true);
                    }else {
                        sortContacts(false);
                    }
                }
            }
        }
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.contact_list, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_item_sort_asc:
                sortContacts(true);
                break;
            case R.id.menu_item_sort_des:
                sortContacts(false);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void sortContacts(final boolean ascending) {
        Collections.sort(contactList, new Comparator<Contact>() {
            @Override
            public int compare(Contact o1, Contact o2) {
                if (ascending){
                    contactsSorted = Sorted_Asc;
                    return o1.getDisplayName().compareTo(o2.getDisplayName());
                }else {
                    contactsSorted = Sorted_Des;
                    return o2.getDisplayName().compareTo(o1.getDisplayName());
                }
            }
        });
        recyclerView.getAdapter().notifyDataSetChanged();
    }

    private ArrayList<Contact> getUDummyUsersData() {
        Contact contact = new Contact();
        contact.setDisplayName("Ali Hassan");
        contact.setEmail("ali@hassan.com");
        ArrayList<Contact> contacts = new ArrayList<>();
        contacts.add(contact);
        contacts.add(contact);
        return contacts;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        if (contactsSorted.length() > 0){
            outState.putString(CONTACTS_SORTED_STATE, contactsSorted);
        }
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public ArrayList<Contact> getContactsData() {
        if (contactList != null){
            return contactList;
        }
        Bundle dataBundle = getArguments();
        if (dataBundle == null){
            return new ArrayList<>();
        }
        String contactsData = dataBundle
                .getString(getString(R.string.contact_framgnet_data_arg), "");
        contactList = DataHelper.getContactList(contactsData);
        return contactList;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction(Contact item);
    }
}
