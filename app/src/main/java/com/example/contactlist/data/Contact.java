package com.example.contactlist.data;

import org.json.JSONObject;

/**
 * Created by mhassan on 1/7/17.
 */

public class Contact {

    private final JSONObject jsonData;
    private String displayName = "";
    private String email = "";
    private String phone = "";
    private String address = "";
    private String website = "";
    private String company = "";

    public Contact() {
        jsonData = new JSONObject();
    }

    public String getPhone() {
        return phone;
    }

    public String getAddress() {
        return address;
    }

    public String getWebsite() {
        return website;
    }

    public String getCompany() {
        return company;
    }

    private static class JSONFields{
        static final String NAME = "name";
        static final String EMAIL = "email";
        static final String PHONE = "phone";
        static final String ADDRESS = "address";
        static final String WEBSITE = "website";
        static final String COMPANY = "company";
        static final String STREET = "street";
        static final String SUITE = "suite";
        static final String CITY = "city";
        static final String ZIP_CODE = "zipcode";
        static final String COMPANY_NAME = "name";
        static final String CATCH_PHRASE = "catchPhrase";
        static final String COMPANY_BS = "bs";

    }

    public Contact(JSONObject contactObject) {
        jsonData = contactObject;
        try {
            displayName = contactObject.getString(JSONFields.NAME);
            email = contactObject.getString(JSONFields.EMAIL);
            phone = contactObject.getString(JSONFields.PHONE);
            website = contactObject.getString(JSONFields.WEBSITE);
            // set address
            JSONObject addressObject = contactObject.getJSONObject(JSONFields.ADDRESS);
            String street = addressObject.getString(JSONFields.STREET);
            String suite = addressObject.getString(JSONFields.SUITE);
            String city = addressObject.getString(JSONFields.CITY);
            String zipCode = addressObject.getString(JSONFields.ZIP_CODE);
            address = suite +", " + street + ", "+city +", "+zipCode;
            // set company
            JSONObject companyObject = contactObject.getJSONObject(JSONFields.COMPANY);
            String companyName = companyObject.getString(JSONFields.NAME);
            String catchPhrase = companyObject.getString(JSONFields.CATCH_PHRASE);
            String companyBS = companyObject.getString(JSONFields.COMPANY_BS);
            company = companyName + "\n"+ catchPhrase+"\n"+companyBS;
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getEmail() {
        return email;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        if (jsonData.length() > 0){
            return jsonData.toString();
        }
        return super.toString();
    }
}
