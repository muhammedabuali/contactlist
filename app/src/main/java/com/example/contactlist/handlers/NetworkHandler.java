package com.example.contactlist.handlers;

import android.app.Activity;
import android.content.Context;

import com.example.contactlist.Interfaces.ArgRunnable;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by mhassan on 1/7/17.
 */

public class NetworkHandler {

    private static NetworkHandler networkHandler;
    private final OkHttpClient okHttpClient;
    private static final String usersUrl = "http://jsonplaceholder.typicode.com/users";
    private String contactsDataString = null;

    public synchronized static NetworkHandler getInstance(Activity activity){
        if (networkHandler == null){
            networkHandler = new NetworkHandler(activity.getApplicationContext());
            return networkHandler;
        }
        return networkHandler;
    }

    public NetworkHandler(Context applicationContext){
        okHttpClient = new OkHttpClient();
    }

    public synchronized void getUsersAsync(ArgRunnable onSuccess){
        // if I already loaded the data don't fetch it again
        // todo check if data loading was recent if data changes on the server
        if (contactsDataString != null){
            onSuccess.run(contactsDataString);
           return;
        }
        try{
            Request request = new Request.Builder()
                    .url(usersUrl)
                    .build();

            Callback callback = getCallBack(onSuccess);
            okHttpClient.newCall(request).enqueue(callback);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private Callback getCallBack(final ArgRunnable onSuccess) {
        return new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
                contactsDataString = response.body().string();
                onSuccess.run(contactsDataString);
            }
        };
    }
}
