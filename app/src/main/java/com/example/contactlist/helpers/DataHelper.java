package com.example.contactlist.helpers;

import com.example.contactlist.data.Contact;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by mhassan on 1/7/17.
 */
public class DataHelper {
    public static ArrayList<Contact> getContactList(String contactsData) {
        ArrayList<Contact> contacts = new ArrayList<>();
        try {
            JSONArray contactsArray = new JSONArray(contactsData);
            for (int i = 0; i < contactsArray.length(); i++) {
                try{
                    JSONObject contactObject = (JSONObject) contactsArray.get(i);
                    Contact contact = new Contact(contactObject);
                    contacts.add(contact);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return contacts;
    }

    public static Contact getContact(String contactDataString) {
        try{
            JSONObject contactObject = new JSONObject(contactDataString);
            Contact contact = new Contact(contactObject);
            return contact;
        }catch (Exception e){
            e.printStackTrace();
        }
        return new Contact();
    }
}
