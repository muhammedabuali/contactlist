package com.example.contactlist.activities;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.example.contactlist.Interfaces.ArgRunnable;
import com.example.contactlist.R;
import com.example.contactlist.data.Contact;
import com.example.contactlist.fragments.ContactsFragment;
import com.example.contactlist.handlers.NetworkHandler;

public class ContactListActivity extends AppCompatActivity
        implements ContactsFragment.OnListFragmentInteractionListener{

    private static final String Contacts_Visible = "Contacts Visible";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_list);

        // set toolbar title
        setTitle(getString(R.string.activity_contact_list_title));

        // load users data
        if (savedInstanceState != null){
            boolean contactsVisible = savedInstanceState.getBoolean(Contacts_Visible, false);
            if (!contactsVisible){
                // get contacts data
                fetchContacts();
            }
        }else {
            fetchContacts();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putBoolean(Contacts_Visible, true);
        super.onSaveInstanceState(outState);
    }


    private void fetchContacts() {
        // show Progress layout
        View progressLayout = findViewById(R.id.progress_layout);
        if (progressLayout != null){
            progressLayout.setVisibility(View.VISIBLE);
        }
        // make async network requests
        NetworkHandler networkHandler = NetworkHandler.getInstance(this);
        networkHandler.getUsersAsync(onUsersFetched());
    }

    // update view with users data
    private ArgRunnable onUsersFetched() {
        return new ArgRunnable() {
            @Override
            public void run(Object o) {
                showUsersAsync(o);
            }
        };
    }

    private void showUsersAsync(Object o) {
        if (! (o instanceof String)){
            return;
        }
        final String result = (String) o;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                showContactsFragment(result);
            }

            private void showContactsFragment(String data) {
                Fragment contactsFragment = new ContactsFragment();
                // pass fragment data
                Bundle dataBundle = new Bundle();
                dataBundle.putString(
                        getString(R.string.contact_framgnet_data_arg),data);
                contactsFragment.setArguments(dataBundle);
                // hide the progress layout
                View progressLayout = findViewById(R.id.progress_layout);
                if (progressLayout != null){
                    progressLayout.setVisibility(View.GONE);
                }
                //show the fragment
                getSupportFragmentManager().beginTransaction()
                        .add(R.id.activity_contact_list_layout, contactsFragment)
                        .commit();
            }
        });
    }

    @Override
    public void onListFragmentInteraction(Contact item) {
        Intent detailIntent = new Intent(this, ContactDetailActivity.class);
        detailIntent.putExtra(
                getString(R.string.contact_detail_data_arg), item.toString());
        startActivity(detailIntent);
    }
}
