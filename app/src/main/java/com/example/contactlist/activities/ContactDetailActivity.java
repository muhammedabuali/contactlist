package com.example.contactlist.activities;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.example.contactlist.R;
import com.example.contactlist.data.Contact;
import com.example.contactlist.helpers.DataHelper;

public class ContactDetailActivity extends AppCompatActivity {

    private Contact contactData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_detail);

        // display back button
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null){
            supportActionBar.setDisplayHomeAsUpEnabled(true);
        }

        // read contact data
        getContactDetails();

        // show contact data
        if (contactData != null){
            displayContactDetails();
        }

    }

    private void displayContactDetails() {
        String displayName = contactData.getDisplayName();
        // set activity title
        setTitle(displayName);
        // display details
        String[][] fields = new String[][]{
                /*layout id,                value,                          type*/
                {R.id.contact_username+"",  contactData.getDisplayName(),   getString(R.string.contact_detail_username)},
                {R.id.contact_phone+"",     contactData.getPhone(),         getString(R.string.contact_detail_phone)},
                {R.id.contact_address+"",   contactData.getAddress(),       getString(R.string.contact_detail_address)},
                {R.id.contact_website+"",   contactData.getWebsite(),       getString(R.string.contact_detail_website)},
                {R.id.contact_company+"",   contactData.getCompany(),       getString(R.string.contact_detail_company)},
        };

        for (int i = 0; i < fields.length; i++) {
            try{
                String[] row = fields[i];
                View fieldLayout = findViewById(Integer.parseInt(row[0]));
                // set field value
                TextView titleView = (TextView) fieldLayout.findViewById(R.id.title);
                titleView.setText(row[1]);
                // set field type
                TextView typeView = (TextView) fieldLayout.findViewById(R.id.body);
                typeView.setText(row[2]);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    private void getContactDetails() {
        Bundle extras = getIntent().getExtras();
        if (extras == null){
            return;
        }
        String contactDataString = extras.getString(
                getString(R.string.contact_detail_data_arg), "");
        if (contactDataString.length() == 0){
            return;
        }
        contactData= DataHelper.getContact(contactDataString);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
